ABOUT THE DATASET
===============================================================================

Content
===============================================================================
Simple directory structure branched into test and train and further branched into the respective 3 classes which contains the images.

Context
===============================================================================
Created this repository for educational purposes.

Acknowledgements
===============================================================================
The University of Montreal for releasing the images 

License
===============================================================================
CC BY-SA 4.0 (https://creativecommons.org/licenses/by-nc-sa/4.0/)

Source
===============================================================================
https://www.kaggle.com/datasets/pranavraikokte/covid19-image-dataset
